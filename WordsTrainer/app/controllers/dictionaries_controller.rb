class DictionariesController < ApplicationController
  def index
    @dictionary = Dictionary.all
  end

  def new
    @dictionary = Dictionary.new
  end

  def create
    @dictionary = Dictionary.new(params[:dict_name])
    @dictionary.save
  end
end
